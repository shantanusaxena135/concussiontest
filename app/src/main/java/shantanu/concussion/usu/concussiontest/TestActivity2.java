package shantanu.concussion.usu.concussiontest;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.Vibrator;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;

import static java.lang.Math.sin;


public class TestActivity2 extends Activity implements SensorEventListener {

    public static int nwidth, nheight;

    // accelerometer values, ball coordinate calculations and filter variables
    public static float xPosition = 0.0f, yPosition = 0.0f, oldx = 0.0f, oldy = 0.0f;
    private float xAcceleration = 0.0f, xVelocity = 0.0f;
    private float yAcceleration = 0.0f, yVelocity = 0.0f;
    double tvarX = 0.1f;
    double tvarY = 0.5f;
    private float zAcceleration = 0.0f;
    public float frameTime = 0.999f;
    private float mAlpha = 0.9f;
    private static final String TAG = "Readings";

    // sensor variables
    SensorManager sensorManager = null;
    private Sensor Accelerometer = null;

    //gesture recognition, used to detect sliding
    private GestureDetector gDetector;

    //booleans for controlling flow
    Boolean doneSaving = false;
    Boolean practiceMode = false;
    Boolean doSave = true;
    Boolean trace = true;

    //Drawing variables
    DrawingView mView;
    private Paint mPaint;
    public ImageView ball;
    public ArrayList<PathWithPaint> _graphics3 = new ArrayList<PathWithPaint>();

    String toWrite = "";
    Bundle extras;
    int Magnification = 50;
    Vibrator v = null;



    // The entry function for the activity
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        setContentView(R.layout.activity_test);

        extras = getIntent().getExtras();

        //setup test screen
        getScreenSize();
        validateTrial();
        resetball(true);
     //   validateMF();
        setupCanvas();
        v = (Vibrator) getApplicationContext().getSystemService(Context.VIBRATOR_SERVICE);

    }

    private void setupCanvas()
    {
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        RelativeLayout layout = (RelativeLayout) findViewById(R.id.testScreen);
        init();
        mView = new DrawingView(this, 1.0f, nheight / 2, extras.getString("Curve"));
        layout.addView(mView, new ViewGroup.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.MATCH_PARENT));
    }

    private void init() {
        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(Color.BLACK);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(8);
    }

    private void validateTrial() {
        if (extras.getString("TestType").equals("PracTest"))
            practiceMode = true;
    }



    private void resetball(Boolean clearData) {
        ball = (ImageView) findViewById(R.id.image_ball);
        xPosition = 0.0f;
        yPosition = 0.0f;
        TranslateAnimation animation = new TranslateAnimation(oldx, xPosition, oldy, yPosition);
        animation.setDuration(1);
        animation.setFillAfter(false);
        //    animation.setAnimationListener(new MyAnimationListener(ball));

        ball.startAnimation(animation);
        if (clearData) toWrite = "";
    }


    private void unRegisterSensor() {
        sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        Accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        sensorManager.unregisterListener(this);
        trace = false;

    }

    private void validateMF() {
        switch (extras.getString("Magnification")) {
            case "MF1":
                Magnification = 1;
                break;
            case "MF50":
                Magnification = 50;
                break;
            case "MF100":
                Magnification = 100;
                break;
            case "MF150":
                Magnification = 150;
                break;
            case "MF200":
                Magnification = 200;
                break;
        }
        Log.i("Magnification", "Magnification:" + Magnification);
    }



    private void getScreenSize() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        nheight = displaymetrics.heightPixels;
        nwidth = displaymetrics.widthPixels;
        Log.i(TAG, "width" + nwidth + ": height" + nheight);
    }

    private void registerSensor() {
        toWrite = "";
        Toast.makeText(getApplicationContext(), "Get Ready", Toast.LENGTH_SHORT).show();
            sensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
            Accelerometer = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            sensorManager.registerListener(this, Accelerometer, SensorManager.SENSOR_DELAY_FASTEST);
            _graphics3 = new ArrayList<PathWithPaint>();
            trace = true;

    }


    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            yAcceleration = lowPass(sensorEvent.values[1], yAcceleration);
            xAcceleration = lowPass(sensorEvent.values[2], xAcceleration);
            zAcceleration = lowPass(sensorEvent.values[0], zAcceleration);
            Log.i(TAG, "x:" + xAcceleration + " y:" + yAcceleration + "z:" + zAcceleration);
            updateBall();
        }
    }


    float lowPass(float current, float filtered) {
        return mAlpha * current + (1.0f - mAlpha) * filtered;
    }


    private void updateBall() {
        xVelocity = (zAcceleration * frameTime);
        yVelocity = (-yAcceleration * frameTime);

        //Calc distance travelled in that time
        float xS = (xVelocity) * frameTime;
        float yS = (yVelocity) * frameTime;

        //device locked in landscape mode
        xPosition += 0.78;
        yPosition += xS * Magnification;
        setPosition();
        Log.i(TAG, "x:" + tvarX + " y:" + tvarY);
    }


    private void setPosition() {
        if(!practiceMode)
            toWrite += new Timestamp(new java.util.Date().getTime()).toString().substring(17) + "," + xAcceleration + "," + yAcceleration + "," + zAcceleration + "," + xPosition + "," + yPosition + "\n";
          // toWrite = toWrite + xPosition + "," + yPosition +  "\n";

        checkEdges();
        ball = (ImageView) findViewById(R.id.image_ball);
        TranslateAnimation animation = new TranslateAnimation(oldx, xPosition, oldy, yPosition);
        animation.setDuration(1000);
        animation.setFillAfter(false);
        ball.startAnimation(animation);
        mView.drawTrace();
    }


    private void checkEdges() {
        if (yPosition >= nheight - 150) {
            yPosition = nheight - 150;
        } else if (yPosition <= 0) {
            yPosition = 0;
        }
        if (xPosition >= nwidth - 250) {
            xPosition = nwidth - 250;
            endResult();
        } else if (xPosition <= 0) {
            xPosition = 0;
        }
    }


    private void endResult() {
        trace = false;
        v.vibrate(800);
        doSave = true;
        writeCSV(toWrite);
    }


    private void writeCSV(String txtData) {
        if (!practiceMode && !doneSaving && doSave) {
            String TesterID = extras.getString("TesterID");
            if (TesterID.equals("")) TesterID = "NoID";
            String FileName = TesterID + "-" + extras.getString("Curve") + "-" + extras.getString("TestType") + "-" + extras.getString("TestPosition") + "-" + extras.getString("Magnification") + ".txt";

            try {
                File root = new File(Environment.getExternalStorageDirectory(), "Test Readings");
                if (!root.exists()) {
                    root.mkdirs();
                }
                File gpxfile = new File(root, FileName);
                FileWriter writer = new FileWriter(gpxfile, false);
             //   txtData = txtData.substring(4);

                writer.append(txtData);
                writer.flush();
                writer.close();
                Toast.makeText(this, "Data Saved", Toast.LENGTH_SHORT).show();
                doneSaving = true;
                resetball(true);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(this, "Unable to Save Data", Toast.LENGTH_SHORT).show();
            }
        }
        unRegisterSensor();
        resetball(false);
    }


    @Override
    public void onStop() {
        super.onStop();
        unRegisterSensor();
    }



    // necessary override to use accelerometer sensor
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }


    //nested class to draw on canvas along with calculation of
    //points and controlling other functionality of activity
    class DrawingView extends View {

        //canvas drawing variables
        private Path path;
        private Bitmap mBitmap, swipeBitmap, refreshBitmap;
        public Canvas mCanvas;
        PathWithPaint pp = new PathWithPaint();
        Paint lPaint, tPaint;
        private ArrayList<PathWithPaint> _graphics1 = new ArrayList<PathWithPaint>();
        private ArrayList<PathWithPaint> _graphics2 = new ArrayList<PathWithPaint>();
        private ArrayList<float[]> _graphics3 = new ArrayList<float[]>();

        //position variables for trace and ideal curve
        float tvarX, tvarY, nheight, nwidth;
        float width, height;
        float newVarY;

        //booleans to setup drawing parameters
        boolean sine = false, line = false;
        Boolean startDrawing = false;
        
        String sineString, curve;


        public DrawingView(Context context, float x, float y, String curve) {
            super(context);
            this.curve = curve;
            initializeCurve(curve);
            tvarX = x;
            tvarY = y;

            initializeCanvas();
            getScreenSize();
            InitializePaint();
            startDrawing();
            setupControlsBitmaps();
        }


        private void initializeCanvas()
        {
            path = new Path();
            mBitmap = Bitmap.createBitmap(820, 480, Bitmap.Config.ARGB_8888);
            mCanvas = new Canvas(mBitmap);
            this.setBackgroundColor(Color.TRANSPARENT);
        }


        private void initializeCurve(String curve) {
            switch (curve) {
                case "sine":
                    width = 25;
                    height = 150;
                    sine = true;
                    break;

                case "line":
                    // width=0;
                    //  height=0;
                    line = true;
                    break;

            }

        }

        private void setupControlsBitmaps()
        {
            Bitmap swipe = BitmapFactory.decodeResource(getResources(), R.drawable.swipe);
            swipeBitmap = Bitmap.createScaledBitmap(swipe, 100, 500, true);

            Bitmap refresh = BitmapFactory.decodeResource(getResources(), R.drawable.refresh);
            refreshBitmap = Bitmap.createScaledBitmap(refresh, 150, 150, true);

        }


        public void drawTrace() {
            Log.d("ThreadTrace", "Thread Active" + trace.toString());
            if (trace) {
                if (xPosition >= 237.0f) {
                    float[] point = new float[2];
                    point[0] = xPosition;
                    point[1] = yPosition;

                    _graphics3.add(point);
                    tPaint = new Paint();
                    tPaint.setColor(Color.RED);
                    tPaint.setStyle(Paint.Style.FILL_AND_STROKE);
                    invalidate();
                }
                oldx = xPosition;
                oldy = yPosition;

            }
        }



        private void drawAxis() {
            pp = new PathWithPaint();
            lPaint = new Paint();
            path = new Path();
            lPaint.setColor(Color.BLACK);
            lPaint.setStyle(Paint.Style.STROKE);
            mCanvas.drawPath(path, lPaint);
            path.moveTo(237.0f, 0);
            path.lineTo(237.0f, nheight);
            if (line) {
                path.moveTo(0.0f, nheight / 2);
                path.lineTo(nwidth, nheight / 2);
                getlineFile();
            }
            pp.setPath(path);
            pp.setmPaint(lPaint);
            _graphics1.add(pp);
            invalidate();
        }


        private void InitializePaint() {
            mCanvas.drawPath(path, mPaint);
            drawAxis();
            path.moveTo(0.0f, nheight / 2);
            pp.setPath(path);
            pp.setmPaint(mPaint);
            _graphics1.add(pp);
            invalidate();
        }

        private void startDrawing() {
            startDrawing = true;
            if (sine) {
                while (startDrawing) {
                    tvarY =  ((height * (float) sin(((double) tvarX / width))) + nheight / 2);
                    tvarX += 5;
                    drawCurve();

                    if (tvarX >= nwidth - 250)
                        startDrawing = false;
                        getSineFile();

                }
            }

        }

        // to generate file containing coordinate values for straight line on the screen
        public void getlineFile()
        {
            float pixelCount =0.0f;
            String lineString="";

            while (pixelCount<nwidth-250)
            {
                pixelCount+= 0.78;
                lineString+= pixelCount + "," + nheight/2 + "\n";
                Log.d("line file", "Calculating Line");
            }

            String FileName = "sineString-line.txt";
            try {
                File root = new File(Environment.getExternalStorageDirectory(), "Ideal Readings");
                if (!root.exists()) {
                    root.mkdirs();
                }
                File gpxfile = new File(root, FileName);
                FileWriter writer = new FileWriter(gpxfile, false);


                writer.append(lineString);
                writer.flush();
                writer.close();
                //  Toast.makeText(getApplicationContext(), "Line Data Saved", Toast.LENGTH_SHORT).show();
                // doneSaving = true;
                // resetball(true);
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Unable to Save Line Data", Toast.LENGTH_SHORT).show();
            }
        }


        // to generate file containing coordinate values for sine wave curve on the screen
        private void getSineFile()
        {
            try {
                File root = new File(Environment.getExternalStorageDirectory(), "Ideal Readings");
                if (!root.exists()) {
                    root.mkdirs();
                }
                File gpxfile = new File(root, "Ideal Readings - " + curve + ".txt");
                FileWriter writer = new FileWriter(gpxfile, false);
               // sineString = sineString.substring(4);
                writer.append(sineString);
                writer.flush();
                writer.close();
               //   Toast.makeText(getApplicationContext(), "Sine Data Saved", Toast.LENGTH_SHORT).show();
            } catch (IOException e) {
                e.printStackTrace();
                Toast.makeText(getApplicationContext(), "Unable to Save Data", Toast.LENGTH_SHORT).show();
            }

        }

        private void getScreenSize() {
            DisplayMetrics displaymetrics = new DisplayMetrics();
            getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
            nheight = displaymetrics.heightPixels;
            nwidth = displaymetrics.widthPixels;
        }




        @Override
        public boolean onTouchEvent(MotionEvent event) {
            if (event.getX() > nwidth - 400 && event.getX() < nwidth - 250 && event.getY() > 30 && event.getY() < 170) {
                unRegisterSensor();
                resetball(true);
                _graphics3 = new ArrayList<float[]>();
                invalidate();
                v.vibrate(200);
                return true;
            }

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                _graphics3 = new ArrayList<float[]>();
                resetball(true);
                registerSensor();
            }
            invalidate();
            return true;
        }

        @Override
        protected void onDraw(Canvas canvas) {
            super.onDraw(canvas);
            canvas.drawBitmap(swipeBitmap, 0.0f, 100f, null);
            canvas.drawBitmap(refreshBitmap, (float) (nwidth - 400), 30f, null);

            if (_graphics1.size() > 0) {
                canvas.drawPath(
                        _graphics1.get(_graphics1.size() - 1).getPath(),
                        _graphics1.get(_graphics1.size() - 1).getmPaint());
            }

            if (_graphics2.size() > 0) {
                canvas.drawPath(
                        _graphics2.get(_graphics2.size() - 1).getPath(),
                        _graphics2.get(_graphics2.size() - 1).getmPaint());
            }

            if (_graphics3.size() > 0) {
                for (float[] f : _graphics3) {
                    canvas.drawCircle(f[0], f[1], 5, tPaint);
                    if (f[0] > nwidth - 260) _graphics3 = new ArrayList<>();
                }
            }
        }


        public void drawCurve() {

            newVarY = nheight - tvarY;
            sineString = sineString + tvarX + "," + newVarY + "\n";
            pp = new PathWithPaint();
            mPaint = new Paint();
            mPaint.setColor(Color.BLACK);
            mPaint.setStyle(Paint.Style.STROKE);
            mCanvas.drawPath(path, mPaint);
            if (tvarY == 0) return;
            Log.d("Readings", "x: " + tvarX + "  y:" + tvarY);
            path.lineTo(tvarX, tvarY );
            pp.setPath(path);
            pp.setmPaint(mPaint);
            _graphics2.add(pp);
            invalidate();
        }
    }


}