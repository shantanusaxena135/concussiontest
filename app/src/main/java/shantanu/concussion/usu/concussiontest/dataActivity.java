package shantanu.concussion.usu.concussiontest;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.RadioGroup;
import android.widget.TextView;


public class dataActivity extends Activity {

    Intent TestActivityIntent;
    Bundle extras;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        super.onCreate(savedInstanceState);
        extras = getIntent().getExtras();
        setContentView(R.layout.activity_data);
        TextView txtWelcome = (TextView) findViewById(R.id.textWelcome);
        txtWelcome.setText("Welcome " + extras.getString("TesterID") + "..");
        TestActivityIntent= new Intent(this, TestActivity2.class);
        setupControls();
    }


    void setupControls() {

        final RadioGroup rgCurve = (RadioGroup) findViewById(R.id.CurveRadiogroup);
        final RadioGroup rgTrial = (RadioGroup) findViewById(R.id.TrialRadiogroup);
        final RadioGroup rgPS = (RadioGroup) findViewById(R.id.SitStandRadiogroup);
       // final RadioGroup rgMF = (RadioGroup) findViewById(R.id.MFRadioGroup);

        Button go = (Button) findViewById(R.id.buttonStart);


      /*  rgTrial.animate().rotation(90).start();
        rgCurve.animate().rotation(90).start();
        go.animate().rotation(90).start(); */


        go.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String Curve = getResources().getResourceEntryName(rgCurve.getCheckedRadioButtonId());
                String TestType = getResources().getResourceEntryName(rgTrial.getCheckedRadioButtonId());
            //    String Magnification = getResources().getResourceEntryName(rgMF.getCheckedRadioButtonId());
                String TesterID = extras.getString("TesterID");
                String testPosition = getResources().getResourceEntryName(rgPS.getCheckedRadioButtonId());
                TestActivityIntent.putExtra("Curve", Curve);
                TestActivityIntent.putExtra("TestType", TestType);
                TestActivityIntent.putExtra("TestPosition", testPosition);
           //     TestActivityIntent.putExtra("Magnification", Magnification);
                TestActivityIntent.putExtra("TesterID", TesterID);

                startActivity(TestActivityIntent);
            }


        });


    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


}