package shantanu.concussion.usu.concussiontest;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioGroup;
import android.widget.Toast;


public class MainActivity extends Activity {

       Intent DataActivityIntent;
       EditText TesterID =null;
       Button buttonNav =null;
       String idString="No-ID";
     //  int c=0;


       @Override
       protected void onCreate(Bundle savedInstanceState) {
           setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
           super.onCreate(savedInstanceState);
           setContentView(R.layout.activity_main);
           DataActivityIntent= new Intent(this, dataActivity.class);
           setupControls();
       }


       public void setupControls()
       {
           TesterID = (EditText) findViewById(R.id.TesterId);
           buttonNav = (Button) findViewById(R.id.buttonNav);
          // TesterID.animate().rotation(90).start();
          // buttonNav.animate().rotation(90).start();

           buttonNav.setOnClickListener(new View.OnClickListener() {
               @Override
               public void onClick(View v) {
                   try {
                       idString = TesterID.getText().toString();
                   } catch (Exception e) {
                       Toast.makeText(getParent(), "Enter Identifier Text", Toast.LENGTH_SHORT).show();
                   }
                   DataActivityIntent.putExtra("TesterID", idString);
                 //  DataActivityIntent.putExtra("TestCounter",Integer.toString(c) );
                   startActivity(DataActivityIntent);
               }
           });
       }


   }